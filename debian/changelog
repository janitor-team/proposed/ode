ode (2:0.16-3) unstable; urgency=medium

  * Applied patches from upstream to fix build failures in some archs.
    Added __sparc__ define (Thanks jrtc27 in #debian-mentors).
  * Revert disable autotest in some archs.
  * Add pkg-config build dependencies to detect libccd.

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Sun, 13 Jan 2019 13:46:29 +0100

ode (2:0.16-2) unstable; urgency=medium

  * Disable autotest in some arch because they crash.
  * Added a missing license.

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Sun, 13 Jan 2019 01:39:46 +0100

ode (2:0.16-1) unstable; urgency=medium

  * Uploaded version to unstable.
  * Transition initiated to libode8.

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Fri, 11 Jan 2019 19:36:13 +0100

ode (2:0.16-1~exp1) experimental; urgency=medium

  * New upstream version

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Tue, 08 Jan 2019 11:35:49 +0100

ode (2:0.14-2) unstable; urgency=medium

  * Uploaded version to unstable.
  * Transition initiated to libode6.

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Thu, 19 May 2016 09:09:53 +0200

ode (2:0.14-1) experimental; urgency=medium

  * New upstream version. Upstream recover the two number version X.Y
  * Transition initiated to libode6 (upstream jump two numbers)
  * Droped applied patch from upstream
  * Upstream bump soname. Packages adapted
  * Dropped old filename
  * Bump Standards-Version to 3.9.8 (no changes)
  * Updated Vcs-Browser and Vcs-Git fields

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Wed, 27 Apr 2016 08:31:56 +0200

ode (2:0.13.1+git20150309-2) unstable; urgency=medium

  * Uploaded version to unstable.
  * Transition initiated to libode4.

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Tue, 10 Nov 2015 14:54:59 +0100

ode (2:0.13.1+git20150309-1) experimental; urgency=low

  [ Jonathan Dowland ]
  * Team upload.
  * Update Vcs-Browser field in control file. Closes: #705591.

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Anton Balashov ]
  * Fix the watch file (Closes: #705523)

  [ Jackson Doak ]
  * New upstream release. 0.12
  * debian/control:
    - Bump debhelper to 9
    - Build-depend on dh-autoreconf
  * debian/compat: Set as 9
  * debian/rules:
    - Switch to dh
    - Remove manpage lines
    - Enable Hardening
    - Use dh_autoreconf
  * Create PACKAGE.manpage files
  * Create debian/source/format

  [ Leopold Palomo-Avellaneda ]
  * Continue Jackson Doak work ...
  * New upstream release: 0.13.1 with a patch (Closes: #670746)
    Version used git cloned 20150309.
  * New upstream release + git version
  * Added missing dependencies
  * Bump standards to 3.9.6. No changes
  * Added myself as Uploaders
  * Deleted unused files
  * Adapted files to new Soname
  * Rename packages to a new version of Sonames
  * Refactored rules. Conditional options depends on the platform
  * Updated Vcs-Browser and Vcs-Git
  * Drop sp version. Now all platforms have double precision excep
    armel, mips and mipseland that have single precision. Thanks
    Simon McVittie.
  * Install files more simple and better changelog. Thanks to Gianfranco
    Costamagna
  * New watch file for Bitbucket
  * Added patch to solve autoreconf issue. Thanks to James Cowgill and
    Gianfranco Costamagna. Now it's working with dh-autoreconf
  * Dropped unused vars and clean tabs
  * Setting version to experimental
  * Dropped override rule

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Fri, 03 Oct 2014 14:47:43 +0200

ode (2:0.11.1-4.1) unstable; urgency=medium

  [ Aurelien Jarno ]
  * Non-maintainer upload.
  * Build-depends on dh-autoreconf.

  [ Breno Leitao ]
  * Run dh_autoreconf to update autoconf/libtool files (Closes: #752463).

 -- Aurelien Jarno <aurel32@debian.org>  Wed, 17 Sep 2014 22:37:14 +0200

ode (2:0.11.1-4) unstable; urgency=low

  * Team upload.

  [ Gonéri Le Bouder ]
  * improve the description, thanks Josh Triplett (Closes: #539092)

  [ Paul Wise ]
  * Remove Guus Sliepen from uploaders at his request

  [ Evgeni Golov ]
  * Add LIBS="-X11" to the configure call.
    Closes: #555783
  * Add "single precision" to the short descriptions of the sp packages.
  * Drop quilt, it's not used.

 -- Evgeni Golov <evgeni@debian.org>  Wed, 11 May 2011 15:12:46 +0200

ode (2:0.11.1-3) unstable; urgency=low

  * Upload to unstable

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Tue, 28 Jul 2009 21:03:42 +0200

ode (2:0.11.1-2) experimental; urgency=low

  * rules: clean up some old comment
  * provide a single precision flavor:
     add libode-sp-dev and libode1sp (Closes: #534256, #534177)
  * refresh debian/copyright
  * install ode.pc (Closes: #462629)

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 26 Jul 2009 23:31:04 +0200

ode (2:0.11.1-1) unstable; urgency=low

  * New upstream release (Closes: #532054)
  * Use "make distclean" to do the clean up
  * Bump the standard-version to 3.8.2. No change needed.

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Tue, 30 Jun 2009 21:49:35 +0200

ode (2:0.11-4) unstable; urgency=low

  * Upload to unstable

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 15 Mar 2009 15:19:05 +0100

ode (2:0.11-3) experimental; urgency=low

  * libode-dev provides libode0-dev

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Mon, 16 Feb 2009 00:14:13 +0100

ode (2:0.11-2) experimental; urgency=low

  * libode-dev conflicts against libode0-dev, thanks Vincent Bernat

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 15 Feb 2009 23:29:17 +0100

ode (2:0.11-1) experimental; urgency=low

  * New upstream release
  * debian/rules: clean up

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 15 Feb 2009 00:36:34 +0100

ode (2:0.10.1-1) experimental; urgency=low

  * new upstream release
   - remove fix_makeinstall.diff, useless now
  * remove libode-doc package, the documentation is not provided with
    the sources anymore
  * calls dh_makeshlibs before dh_shlibdeps
  * adjust the rules file
  * upstream bumped the soversion
   - adjust the rules
   - rename libode0debian1 to libode1 and libode0-dev to libode-dev
   - drop change_soname.patch
  * standard version 3.8.0
  * enable double precision

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Mon, 08 Dec 2008 00:15:02 +0100

ode (2:0.9-1) unstable; urgency=low

  * use the original ode tarball since, thanks to Hans de Goede who pointed
    out a clause in OPCODE FAQ that make it dfsg OK
    (Closes: #440484, #465464, #431058)
  * update the copyright file
  * bump the epoch since 0.9.dfsg-2 > 0.9-1
  * bump the standard version to 3.7.3
  * do not create the /usr/include/ode/config which was useless

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Wed, 30 Apr 2008 15:30:34 +0200

ode (1:0.9.dfsg-2) unstable; urgency=low

  [ Eddy Petrișor ]
  * fix Vcs-Svn URL to point to svn, not the incomplete ssh location

  [ Goneri Le Bouder ]
  * indent the Uploaders and Build-Depends entries
  * add libglu-dev and libgl-dev as alternative build dependency for Mesa to
    be able to build with Nvidia nvidia-glx-dev (Closes: #475564)
  * remove some file from the diff.gz

 -- Goneri Le Bouder <goneri@rulezlan.org>  Sat, 26 Apr 2008 02:58:35 +0200

ode (1:0.9.dfsg-1) unstable; urgency=low

  [ Barry deFreese ]
  * Add watch file
  * Add myself to uploaders

  [ Gonéri Le Bouder ]
  * New upstream release
  * use binary-indep, dpkg-buildpackage -B doesn't build binary indep
    package anymore
  * add Vcs-Browser
  * Renamed “XS-Vcs-Svn” to “Vcs-Svn”, now officially supported by dpkg.
  * move homepage from the description to the new Homepage field
  * libode 0.5 transition is finished, add the Conflicts and Replaces fields

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat, 24 Nov 2007 19:23:58 +0100

ode (1:0.8.dfsg-3) unstable; urgency=low

  * Apply changes provide by Jiří Paleček about the DEB_BUILD_OPTIONS parsing
    (Closes: #431057)
   - rules: do not parse DEB_BUILD_OPTIONS to find nostrip anymore, lets
     dh_strip decide if we should strip or not the binarys
   - noopt: really exports the correct flags to g++
  * Add a README.Debian to explain why we use GIMPACT instead of OPCODE and
    warn of the missing double precision support (Closes: #431058)
  * add a missing epoch in the shlibs (Closes: #412026)
  * fix /usr/lib/libode.so link to /usr/lib/libode.so.0debian1.8.0

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Fri, 29 Jun 2007 13:54:43 +0200

ode (1:0.8.dfsg-2) unstable; urgency=low

  [ Miriam Ruiz ]
  * Upgraded SONAME from 0.8.0 to 0debian1.0.0 until Upstream decides to take
    care of SONAMEs properly.
  * Changed debian/rules not to ignore make clean errors.
  * Added homepage to description in debian/control.
  * libode0-dev depends on libode0 (>= ${binary:Version})
    (Closes: #429607, #430602)
  * rename libode0c2 package to libode0debian1 to avoid breakage during upgrade from
    libode 0.5

 -- Miriam Ruiz <little_miry@yahoo.es>  Wed, 27 Jun 2007 07:33:58 +0000

ode (1:0.8.dfsg-1) unstable; urgency=low

   * new upstream release
    - remove add_fPIC_flag.diff (patch accepted)
    - update fix_makeinstall.diff to not create the symlink
    - ode 0.8 is not compatible with binary created with ode 0.5. use the -V
    flag to force packages build with this release to use at last
    ode >= 0.8-1, thanks Yaroslav Halchenko (Closes: #412026)
    - dfsg: remove the OPCODE and contrib directorys because of missing license
   * remove --disable-opcode and use new --with-trimesh=gimpact instead
    GIMPACT is used to manage collision (Closes: #383868, #408023)
    - remove fix_dTRIMESH_ENABLED_undef.diff (useless)
   * clean up in debian/rules
   * debian/copyright:
    - Debian Games Team is the new maintainer
    - update the home site url and add a paragraph about GIMPACT
   * new package: libode-doc
   * add XS-Vcs-Svn informations in the control file

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat,  2 Jun 2007 01:12:35 +0200

ode (1:0.7-1) experimental; urgency=low

  [ Gonéri Le Bouder ]
  * new upstream release (Closes: #379791)
   - extend copyright from 2001-2003 to 2001-2006
   - add patches:
    fix_makeinstall.diff: fix the use of DESTDIR. Patch forwarded
    to upstream
    add_fPIC_flag.diff: add missing -fPIC flag
    fix_dTRIMESH_ENABLED_undef.diff: fix the build is configure is
    called with --disable-opcode. Patch forwarded. Fixed in upstream
   - README renamed README.txt
   - CHANGELOG renamed CHANGELOG.txt
  * package co-maintained by the Debian Games Team
   - Guus Sliepen moved from maintainer to uploader
   - add myself in uploader
  * improve the clean target to remove every created files
  * builddeps on quilt
  * install /usr/bin/ode-config
  * compat 5
  * use dh_install
  * Standards-Version: 3.7.2
  * add ode-config.man.sgml
  * add libglu1-mesa-dev and libgl1-mesa-dev in builddeps

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Wed, 11 Oct 2006 15:45:57 +0200

ode (1:0.5-5) unstable; urgency=low

  * Disable OPCODE, it does pointer arithmetic in a very unsafe way and it
    won't compile on 64 bit architectures. Closes: #297924

 -- Guus Sliepen <guus@debian.org>  Fri,  5 Aug 2005 15:04:27 +0200

ode (1:0.5-4) unstable; urgency=low

  * C++ ABI change, rename libode0 to libode0c2
  * Include user-settings file in libode-dev. Closes: #318837

 -- Guus Sliepen <guus@debian.org>  Fri, 22 Jul 2005 14:04:25 +0200

ode (1:0.5-3) unstable; urgency=low

  * Upstream says I can create a libode.so.0, so I'm adding a libode0 package
    and changing libode-dev to libode0-dev. Closes: #286937

 -- Guus Sliepen <guus@debian.org>  Fri, 28 Jan 2005 15:14:32 +0100

ode (1:0.5-2) unstable; urgency=low

  * Enable OPCODE support. Closes: #290620

 -- Guus Sliepen <guus@debian.org>  Mon, 17 Jan 2005 13:53:30 +0100

ode (1:0.5-1) unstable; urgency=low

  * New upstream release. Closes: #251876

 -- Guus Sliepen <guus@debian.org>  Wed,  2 Jun 2004 17:04:53 +0200

ode (0.039-1) unstable; urgency=low

  * New upstream release.
  * Add a PIC version of the static library to libode-dev.

 -- Guus Sliepen <guus@debian.org>  Wed, 23 Jul 2003 15:12:54 +0200

ode (0.035-3) unstable; urgency=low

  * Remove multiline string literals. Closes: #194998

 -- Guus Sliepen <guus@debian.org>  Wed, 28 May 2003 14:55:54 +0200

ode (0.035-2) unstable; urgency=low

  * Proper name for -dev package.

 -- Guus Sliepen <guus@debian.org>  Tue, 18 Feb 2003 23:44:45 +0100

ode (0.035-1) unstable; urgency=low

  * Initial Release. Closes: #180349

 -- Guus Sliepen <guus@debian.org>  Sun,  9 Feb 2003 12:23:52 +0100
